# Pages Previewer

> In-browser SPA handlebars previewer

This repo contains some experiments using handlebars and vanilla JS to create single-page static sites in the browser only using template strings and json. Given the usecase requires pre-generation for static sites, I've included the equivilent code in Go for testing and reference.
