package main

import (
	"encoding/json"
	"fmt"

	"github.com/aymerick/raymond"
)

func main() {
	tpl := `
<div class="entry">
  <h1>{{title}}</h1>
  <div class="body">
    {{body}}
  </div>
  <ul>
  {{#each items}}
    <li>{{this}}</li>
  {{~/each}}
  </ul>
</div>
`

	jsonCtx := `{"title":"My New Post","body":"This is my first post!","items":["apples","oranges"]}`

	ctx := map[string]interface{}{}
	err := json.Unmarshal([]byte(jsonCtx), &ctx)
	if err != nil {
		panic(err)
	}

	result, err := raymond.Render(tpl, ctx)
	if err != nil {
		panic(err)
	}

	fmt.Print(result)
}

